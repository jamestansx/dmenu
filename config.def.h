/* See LICENSE file for copyright and license details. */
/* Default settings; can be overriden by command line. */

static int topbar = 1;                      /* -b  option; if 0, dmenu appears at bottom     */
static int centered = 0;                    /* -c  option; if 1, dmenu appears at center     */
static int numbered = 1;                    /* -n  option; if 1, display number of matches   */
static int multisel = 1;                    /* -I  option; if 1, <C-Enter> output items on exit   */
static unsigned int border_width = 0;       /* -bw option; width of window border            */
static unsigned int lineheight = 0;         /* -h  option; minimum height of a menu line     */
static unsigned int min_lineheight = 8;
static unsigned int min_width = 500;        /* minimum width when centered */
static int vertpad = 10;              /* vertical padding of dmenu */
static int horzpad = 10;              /* horizontal padding of dmenu */
/* -fn option overrides fonts[0]; default X11 font or font set */
static char font[BUFSIZ] = "monospace:size=10";
static const char *fonts[] = {
	font,
	"monospace:size=10"
};
static char *prompt      = NULL;      /* -p  option; prompt to the left of input field */
static char *pchar       = "*";       /* password character */
static float alpha       = 0.90;      /* transparency value */
static char selfgcolor[]    = "#eeeeee";
static char selbgcolor[]    = "#005577";
static char outfgcolor[]    = "#000000";
static char outbgcolor[]    = "#00ffff";
static char numfgcolor[]    = "#bbbbbb";
static char numbgcolor[]    = "#222222";
static char normfgcolor[]   = "#bbbbbb";
static char normbgcolor[]   = "#222222";
static char borderbgcolor[] = "#005577";
static char borderfgcolor[] = "#eeeeee";
static char promptfgcolor[] = "#eeeeee";
static char promptbgcolor[] = "#005577";
static const char *colors[SchemeLast][2] = {
	/*     fg         bg       */
	[SchemeNorm]   = { normfgcolor,   normbgcolor   },
	[SchemeSel]    = { selfgcolor,    selbgcolor    },
	[SchemeOut]    = { outfgcolor,    outbgcolor    },
	[SchemePrompt] = { promptfgcolor, promptbgcolor },
	[SchemeNum]    = { numfgcolor,    numbgcolor    },
	[SchemeBorder] = { borderfgcolor, borderbgcolor },
};
/* -l option; if nonzero, dmenu uses vertical list with given number of lines */
static unsigned int lines      = 0;

static int histnodup = 1;                   /* if 0, record repeated histories */
static unsigned int maxhist = 64;           /* maximum number of history stored */
static char *histprompt = "History";        /* -Hp option; history search mode's prompt */

/*
 * Characters not considered part of a word while deleting words
 * for example: " /?\"&[]"
 */
static const char worddelimiters[] = " ";

/* Xresources preferences to load at startup */
ResourcePref resources[] = {
	{ "font",             STRING,     &font          },
	{ "alpha",            FLOAT,      &alpha         },
	{ "prompt",           STRING,     &prompt        },
	{ "histprompt",       STRING,     &histprompt    },
	{ "selfgcolor",       STRING,     &selfgcolor    },
	{ "selbgcolor",       STRING,     &selbgcolor    },
	{ "outfgcolor",       STRING,     &outfgcolor    },
	{ "outbgcolor",       STRING,     &outbgcolor    },
	{ "numfgcolor",       STRING,     &numfgcolor    },
	{ "numbgcolor",       STRING,     &numbgcolor    },
	{ "normfgcolor",      STRING,     &normfgcolor   },
	{ "normbgcolor",      STRING,     &normbgcolor   },
	{ "promptfgcolor",    STRING,     &promptfgcolor },
	{ "promptbgcolor",    STRING,     &promptbgcolor },
	{ "borderfgcolor",    STRING,     &borderfgcolor },
	{ "borderbgcolor",    STRING,     &borderbgcolor },
	{ "vertpad",          INTEGER,    &vertpad       },
	{ "horzpad",          INTEGER,    &horzpad       },
	{ "maxhist",          INTEGER,    &maxhist       },
	{ "histnodup",        INTEGER,    &histnodup     },
	{ "mwidth",           INTEGER,    &min_width     },
};

